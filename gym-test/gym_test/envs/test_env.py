from math import pi, sin
import numpy as np
import gym
from gym import error, spaces, utils
from gym.utils import seeding

class TestEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self):
        self.max_position = pi
        
        self.low_state = np.array([0.0, 0.0])
        self.high_state = np.array([self.max_position, self.max_position])

        self.action_space = spaces.Box(low=0.0, high=self.max_position, shape=(2,), dtype=np.float32)
        self.observation_space = spaces.Box(low=self.low_state, high=self.high_state, dtype=np.float32)

        self.seed()
        self.reset()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def reset(self):
        # a and b
        # self.constants = np.array([self.np_random.uniform(low=-10, high=10), self.np_random.uniform(low=-10, high=10)])
        self.constants = np.array([1, 1])
        # x and y
        self.state = np.array(self.np_random.uniform(low=0.0, high=self.max_position, size=(2,)))
        return np.array(self.state)

    # action = [x_new, y_new]
    def step(self, action):
        print("action", action)
        limited_action = np.array([min(max(action[0], 0.0), self.max_position), min(max(action[1], 0.0), self.max_position)])

        self.state = limited_action

        done = 1 - sin(self.constants[0]*self.state[0])*sin(self.constants[1]*self.state[1]) <= 0.01

        reward = 0
        if done:
            reward = 1000
        # reward -= (pi/2.0 - action[0])**2 + (pi/2.0 - action[1])**2
        # reward -= 1.0 - sin(self.constants[0]*self.state[0])*sin(self.constants[1]*self.state[1])
        # reward += (sin(self.state[0]))**3 + (sin(self.state[1]))**3
        reward += 10*sin(self.constants[0]*self.state[0])*sin(self.constants[1]*self.state[1])
        reward -= 2.0 if action[0] < 0 or action[0] > self.max_position else 0
        reward -= 2.0 if action[1] < 0 or action[1] > self.max_position else 0

        return self.state, reward, done, {}
        

    def render(self, mode='human'):
        print(self.constants, self.state, self.constants*self.state/pi, sin(self.constants[0]*self.state[0])*sin(self.constants[1]*self.state[1]))

    def close(self):
        pass
