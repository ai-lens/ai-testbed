import numpy as np
import gym
from gym import error, spaces, utils
from gym.utils import seeding

class LensEnv(gym.Env):
    metadata = {'render.modes': ['human']}

    def __init__(self):
        self.min_position = 0.0
        self.max_position = 1.0
        self.var_positions = 1 # I think this was the number
        self.example_solution = [1.0, 0.0, 0.999723, 0.248648, 1.0, 0.762114, 0.688757, 1.0, 0.400159, 0.0983915, 1.0]
        self.accuracy = 0.01
        
        self.low_state = np.array([self.min_position] * self.var_positions)
        self.high_state = np.array([self.max_position] * self.var_positions)

        self.action_space = spaces.Box(low=self.min_position, high=self.max_position, shape=(self.var_positions,),
                dtype=np.float32)
        self.observation_space = spaces.Box(low=self.low_state, high=self.high_state, dtype=np.float32)

        self.seed()
        self.reset()

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def reset(self):
        self.state = np.array(self.np_random.uniform(low=self.min_position, high=self.max_position,
            size=(self.var_positions,)))
        return np.array(self.state)

    def step(self, action):
        print("action", action)
        limited_action = np.array([min(max(action[i], self.min_position), self.max_position)
            for i in range(len(action))])

        self.state = limited_action

        values = self.state.tolist() + self.example_solution[self.var_positions:]

        evaluation = evaluator.evaluate(values)
        print("evaluation", evaluation)

        done = 1.0 - evaluation <= accuracy

        reward = 0
        if done:
            reward = 100
        reward -= 10*evaluation

        return self.state, reward, done, {}
        

    def render(self, mode='human'):
        print(self.state)

    def close(self):
        pass
