from gym.envs.registration import register

register(
        id='Lens-v0',
        entry_point='gym_lens.envs:LensEnv',
)
