if(${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
	message(FATAL_ERROR "DO NOT BUILD in-tree.")
endif()

project(AiSolver)

file(COPY learningagent.py gym-lens DESTINATION ${CMAKE_CURRENT_BINARY_DIR})
