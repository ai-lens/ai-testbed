import gym

import sys
sys.path.append('gym-test')
import gym_test

env = gym.make('Test-v0')
for i in range(20):
    env.reset()
    for t in range(100):
        env.render()
        action = env.action_space.sample()
        obs, reward, done, info = env.step(action)
        if done:
            print("Episode finished after {} timesteps".format(t+1))
            env.render()
            break
env.close()
