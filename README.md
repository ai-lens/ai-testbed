Please work in your own folder, and in your own branch, dont step on eachother's toes.  otherwise feel free to use this as a code dump for ai related elements

----
Amndeep

dependencies: python3, pip3, tensorflow (1.x), keras, keras-rl, and whatever subdependencies they might have

~~compilation: `pip3 install -e gym-test` - this creates a module that can be imported, after that either have the gym-test folder in the same directory as where you're trying to import it (like I have with `randomaction.py`) or have that directory by on the PYTHONPATH somewhere~~

running: `python3 learningagent.py` - this should hopefully just work while printing out a bunch of stuff to the terminal - first bit is gonna be some random jank; next bit is gonna be it training: prints out an action line that specifies how much it'll move the x and the y and then a state line that says what a and b are, what x and y are now, then a*x/pi and b*y/pi and the closer that is to some nice numbers (i think any n or n.5 basically) the better our solution will be (i think), then what sin(a*x)*sin(b*y) (closer to 1.0 the better iirc); last bit is it testing which is similar to what I've already described
